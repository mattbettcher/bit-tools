

Bit-Tools
=========

Allows easy modification of individual bits and runs of bits.

[![pipeline status](https://gitlab.com/mattbettcher/bit-tools/badges/master/pipeline.svg)](https://gitlab.com/mattbettcher/bit-tools/commits/master)

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
bit-tools = "0.1"
```

and this to your crate root:

```rust
extern crate bit-tools;
```
