#![allow(unused_imports)]
use emplacer::BitEmplacer;
    
#[test]
fn set_bit_test() {
    // test unsigned byte
    let mut u: u8 = 17;
    u.set_bit(3, true);
    assert_eq!(u, 25);

    // test signed byte
    let mut i: i8 = -17;
    i.set_bit(3, false);
    assert_eq!(i, -25);
}

#[test]
fn set_bits_test() {
    // test unsigned byte
    let mut u: u8 = 17;
    u.set_bits(7..6, 2);
    assert_eq!(u, 145);

    // test signed byte
    let mut i: i8 = -17;
    i.set_bits(7..6, 0);
    assert_eq!(i, 47);
}


// todo many more tests!!!!