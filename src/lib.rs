//! This crate adds simple bit-manipulation helpers in a convenient trait.
//! It implements several traits on all the base primitives.
//! 
//! *Key features -*
//! 
//! * Fast - uses debug_assert for maximum speed in release mode.
//! 
//! *Examples -*
//! 
//! ```rust
//! # #[allow(unused_imports)]
//! # #[macro_use] extern crate bit_tools;
//! # use bit_tools::retriver::BitRetriver;
//! # use bit_tools::emplacer::BitEmplacer;
//! # fn main() {
//! // easily test or set a specific bit in any primitive
//! let mut byte: u8 = 54;
//! if byte.bit(5) {                // if the 5th bit is set
//!     byte.set_bit(6, true);      // set the 6th
//! }
//! assert_eq!(byte, 118);
//! # }
//! ```
//! 
//! ```rust
//! # #[allow(unused_imports)]
//! # #[macro_use] extern crate bit_tools;
//! # use bit_tools::retriver::BitRetriver;
//! # use bit_tools::emplacer::BitEmplacer;
//! # fn main() {
//! // easily modify specific bits of any primitive value
//! let mut byte: u8 = 54;
//! byte.set_bits(7..3, 33);       // set the 3rd bit 
//! // Note that the 8th bit in byte would be set if this was a larger primitive
//! assert_eq!(byte, 14);
//! # }
//! ```

#![warn(missing_docs)]
#![no_std]

pub mod retriver;
pub mod emplacer;

// tests
mod retriver_tests;
mod emplacer_tests;