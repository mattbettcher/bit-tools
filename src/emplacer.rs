//! Module provides traits and implementions for modifiying the bits in numeric primitives. 

use core::mem;
use core::ops::Range;

/// A trait which provides methods for injecting bits and bit ranges into primitives.
pub trait BitEmplacer<T> {
    /// Sets the value of a bit at the given position.
    /// 
    /// # Example
    /// 
    /// ```
    /// # #[allow(unused_imports)]
    /// # #[macro_use] extern crate bit_tools;
    /// # pub use self::emplacer::BitEmplacer;
    /// # use bit_tools::*;
    /// # fn main() {
    /// let mut value: u8 = 38;
    /// value.set_bit(6, true);          // Set bit 6 in the given byte.
    /// assert_eq!(value, 102);
    /// # }
    /// ```
    ///
    /// # Debug Panics
    /// 
    /// If n is greater then the size of the type you are extracting the bit from. e.g: 
    /// 
    /// ```rust,ignore
    /// # #[allow(unused_imports)]
    /// # #[macro_use] extern crate bit_tools;
    /// # pub use self::emplacer::BitEmplacer;
    /// # use bit_tools::*;
    /// # fn main() {
    /// let mut value:u8 = 17;           
    /// value.set_bit(9, true);          // Can't set bit 9 there are only 0-7
    /// # }
    /// ```
    fn set_bit(&mut self, n: usize, value: bool) -> &mut Self;
    /// Sets the value of a set of contiguous bits at the given range.
    /// 
    /// # Example
    /// 
    /// ```
    /// # #[allow(unused_imports)]
    /// # #[macro_use] extern crate bit_tools;
    /// # pub use self::emplacer::BitEmplacer;
    /// # use bit_tools::*;
    /// # fn main() {
    /// let mut byte: u8 = 54;
    /// byte.set_bits(7..3, 0b1_0001);
    /// assert_eq!(byte, 142);
    /// # }
    /// ```
    ///
    /// # Debug Panics
    /// 
    /// If Least Significant Bit (LSB) is greater then Most Significant Bit (MSB) first.
    /// 
    /// ```rust,ignore
    /// # #[allow(unused_imports)]
    /// # #[macro_use] extern crate bit_tools;
    /// # pub use self::emplacer::BitEmplacer;
    /// # use bit_tools::*;
    /// # fn main() {
    /// let mut value:u8 = 17;
    /// value.set_bits(0..4, 1);         // Range must be MSB > LSB
    /// # }
    /// ```
    fn set_bits(&mut self, range: Range<usize>, value: T) -> &mut Self;
}

#[doc(hidden)]
#[macro_export]
macro_rules! bitemplacer_impl {
    ($($t:ty),*) => {$(
        impl BitEmplacer<$t> for $t {

            #[inline]
            fn set_bit(&mut self, n: usize, value: bool) -> &mut $t {
                debug_assert!(n <= mem::size_of::<$t>() * 8 - 1, "Bit exceeds u8's number of bits!");
                *self &= !(1 << n);     // clear the bit first
                *self |= (value as $t) << n;
                self
            }

            #[inline]
            fn set_bits(&mut self, range: Range<usize>, value: $t) -> &mut $t {
                debug_assert!(range.start < mem::size_of::<$t>() * 8, "Most Significant Bit is larger then u8!");
                debug_assert!(range.end <= range.start, "Least Signifcant Bit is larger then Most Significant Bit");
                let bit_range = (range.start - range.end) + 1;
                let mask = ((1 << bit_range) - 1) << range.end;
                *self &= !(mask as $t);     // clear the bits first
                *self |= ((value << range.end) as $t) & mask;   // set bits
                self
            }
            
        }
        
    )*}
}

bitemplacer_impl!(u8, u16, u32, u64, usize, i8, i16, i32, i64, isize);