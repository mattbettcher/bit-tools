//! Module provides traits and implementions for extracting the bits in numeric primitives. 

use core::mem;
use core::ops::Range;

/// A trait which provides methods for extracting bits and bit ranges.
pub trait BitRetriver<T> {
    /// Obtains the value of the bit at the given position.
    /// 
    /// # Example
    /// 
    /// ```
    /// # #[macro_use] extern crate bit_tools;
    /// # pub use self::retriver::BitRetriver;
    /// # use bit_tools::*;
    /// # fn main() {
    /// let value: u8 = 172;
    /// if value.bit(5) {          // If the bit is set return true
    ///     // bit 5 is set
    /// }
    /// # }
    /// ```
    ///
    /// # Debug Panics
    /// 
    /// If n is greater then the size of the type you are extracting the bit from. e.g: 
    /// 
    /// ```rust,ignore
    /// # #[macro_use] extern crate bit_tools;
    /// # pub use self::retriver::BitRetriver;
    /// # use bit_tools::*;
    /// # fn main() {
    /// let value:u8 = 17;
    /// value.bit(9);               // Can't get bit 9 there are only 0-7
    /// }
    /// ```
    fn bit(&self, n: usize) -> bool;
    /// Obtains the value of a set of contiguous bits at the given position.
    /// 
    /// # Example
    /// 
    /// ```
    /// # #[macro_use] extern crate bit_tools;
    /// # pub use self::retriver::BitRetriver;
    /// # use bit_tools::*;
    /// # fn main() {
    /// let value: u8 = 54;
    /// let other = value.bits(2..1);     // get bits 
    /// # }
    /// ```
    ///
    /// # Debug Panics
    /// 
    /// If Least Significant Bit (LSB) is greater then Most Significant Bit (MSB) first.
    /// 
    /// ```rust,ignore
    /// # #[macro_use] extern crate bit_tools;
    /// # pub use self::retriver::BitRetriver;
    /// # use bit_tools::*;
    /// # fn main() {
    /// let value:u8 = 17;
    /// value.bits(0..4);            // Range must be MSB > LSB
    /// # }
    /// ```
    fn bits(&self, Range<usize>) -> T;
}

#[doc(hidden)]
#[macro_export]
macro_rules! bitretriver_impl {
    ($($t:ty),*) => {$(
        impl BitRetriver<$t> for $t {

            #[inline]
            fn bit(&self, n: usize) -> bool {
                debug_assert!(n <= mem::size_of::<$t>() * 8 - 1, "Bit exceeds u8's number of bits!");
                (*self & (1 << n)) == 1 << n
            }

            #[inline]
            fn bits(&self, range: Range<usize>) -> $t {
                debug_assert!(range.start < mem::size_of::<$t>() * 8, "Most Significant Bit is larger then u8!");
                debug_assert!(range.end <= range.start, "Least Signifcant Bit is larger then Most Significant Bit");
                let bit_range = (range.start - range.end) + 1;
                let type_mask = <$t>::max_value();
                let mask = (((1 << bit_range) - 1) << range.end) & type_mask;
                (*self & mask) >> range.end
            }
        }
    )*}
}

bitretriver_impl!(u8, u16, u32, u64, usize, i8, i16, i32, i64, isize);