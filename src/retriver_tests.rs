#![allow(unused_imports)]
use retriver::BitRetriver;
    
#[test]
fn bit_test() {
    // unsigned byte test
    let u: u8 = 17;
    assert_eq!(u.bit(4), true);

    // signed byte test
    let  i: i8 = -17;
    assert_eq!(i.bit(7), true);
}

#[test]
fn bits_test() {
    // unsigned byte test
    let u: u8 = 17;
    assert_eq!(u.bits(6..4), 1);

    // signed byte test
    let  i: i8 = -17;
    assert_eq!(i.bits(4..2), 3);
}

// todo many more tests!!!!